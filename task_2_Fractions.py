# Create a function what takes two parameters of string type
# which are fractions with the same denominator and
# returns a sum expression of these fractions and the sum result. For example:

# >>> a_b = '1/3'
# >>> c_b = '5/3'
# >>> get_fractions(a_b, c_b)
# '1/3 + 5/3 = 6/3'

# Создайте функцию, которая принимает два параметра строкового типа,
# которые являются дробями с одинаковым знаменателем, и возвращает
# выражение суммы этих дробей и результат суммы. Например:

 def get_fractions(a_b: str, c_b: str) -> str:
    a = int(a_b[0])
    c = int(c_b[0])
    b = int(a_b[2])
    
    return f'{a}/{b} + {c}/{b} = {a + c}/{b}'






