# Create a function which generates and returns a list with numbers
# from 1 to n inclusive where the n is passed as a parameter to the
# function. But if the number is divided by 3 the function puts a Fizz word into the list,
# and if the number is divided by 5 the function puts a Buzz word into the list.
# If the number is divided both by 3 and 5 the function puts FizzBuzz into the list.

# Создайте функцию, которая генерирует и возвращает
# список с числами от 1 до n включительно,
# который затем передается в качестве параметра функции.
# Но если число делится на 3, функция помещает в список
# Шипящее слово, а если число делится на 5, функция помещает в список Модное слово.
# Если число делится как на 3, так и на 5, функция помещает FizzBuzz в список.

def function(n):
    around = list(range(1, n + 1))

    for i in around:
        if i % 3 == 0 and i % 5 == 0:
            i = 'FizzBuzz'
        elif i % 3 == 0:
            i = 'Fizz'
        elif i % 5 == 0:
            i = 'Buzz'
        print(i, end=', ')

txt = int(input('n= '))
function(txt)

