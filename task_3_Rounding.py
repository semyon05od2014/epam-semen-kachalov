# Create a function with two parameters a and b.
# The function calculates the following expression:
# (12 * a + 25 * b) / (1 + a**(2**b))
# and returns a result of the expression rounded up to the second decimal place.

# Создайте функцию с двумя параметрами a и b.
# Функция вычисляет следующее выражение:
# (12 * a + 25 * b) / (1 + a**(2**b))
# и возвращает результат выражения, округленный до второго знака после запятой.

def rounding(a,b):
    y = (12 * a + 25 * b) / (1 + a**(2**b))
    x = round(y, 2)
    return x

a = float(input('a = '))
b = float(input('b = '))
print(rounding(a, b))
